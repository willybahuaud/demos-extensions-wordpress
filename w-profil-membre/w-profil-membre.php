<?php

/**
 * Plugin name: Profil membre
 * Plugin URI: https://plugin.example.com
 * Description: une extension pour gérer un profil membre en front
 * Author : toi
 * Author URI: https://example.com
 * Contributors: toi, moi
 * Text Domain: w-profil-membre
 * Version: 0.1
 * Stable tag: 0.1
 */

/**
 * Bloquer les accès directs
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html__( 'Cheatin&#8217; uh?' ) );
}

define( 'W_DEMO_PROFIL_MEMBRE', 0.1 );

/**
 * Charger les ressources
 * 
 * @see https://developer.wordpress.org/reference/functions/wp_register_style/
 */
add_action( 'wp_enqueue_scripts', 'andon_register_scripts' );
function andon_register_scripts() {
	wp_register_style( 'profil-style', plugins_url( 'style.css', __FILE__ ), false, W_DEMO_PROFIL_MEMBRE );
}

/**
 * Ajouter le nouveau type d’utilisateurs
 *
 * @see https://codex.wordpress.org/Function_Reference/register_activation_hook
 */
register_activation_hook( __FILE__, 'w_demo_register_role_on_plugin_activation' );
function w_demo_register_role_on_plugin_activation() {
    add_role( 'member', __( 'Membre', 'annuaire-wabeo' ), array( 'read' => true, 'level_0' => true ) );
}

/**
 * Ajout d’options de lecture pour la page de profil
 * 
 * @see https://developer.wordpress.org/plugins/settings/settings-api/
 */
add_action( 'admin_init', 'w_demo_settings_api_init' );
function w_demo_settings_api_init() {
	register_setting( 'reading', 'profil-page' );

	add_settings_field(
		'profil-page',
		__( 'Page de profil', 'w-profil-membre' ),
		'wp_dropdown_pages',
		'reading',
		'default',
		array(
			'selected' => get_option( 'profil-page' ),
			'name'     => 'profil-page',
		)
	);
}

/**
 * Ajout du formulaire sur la page de profil
 */
add_filter( 'the_content', 'w_demo_profil_page' );
function w_demo_profil_page( $content ) {
	if ( is_page( get_option( 'profil-page', 999 ) ) ) {
    	wp_enqueue_style( 'profil-style' );
    	$content .= is_user_logged_in() ? w_demo_form( 'existe' ) : w_demo_login() . w_demo_form();
	}
	return $content;
}

/**
 * Formulaire de connexion
 *
 * @see https://developer.wordpress.org/reference/functions/wp_login_form/
 */
function w_demo_login() {
    $out = array( '<div class="w_demo-login-form">' );
    $out[] = '<h2>' . __( 'Vous êtes déjà inscrit ? Connectez-vous !', 'w-profil-membre' ) . '</h2>';
    $out[] = wp_login_form( array(
        'echo'     => false,
        'remember' => false,
        'redirect' => get_permalink( get_option( 'profil-page' ) ),
    ) );
    $out[] = '</div>';
    return implode( PHP_EOL, $out );
}

/**
 * Fonction utile pour générer tout types de champs
 */
function w_demo_field( $args ) {
    if ( empty( $args['type'] ) ) {
        $args['type'] = 'text';
    }
    switch ( $args['type'] ) {
        case 'textarea':
            return vsprintf( '<p><label for="%1$s">%4$s%6$s</label><textarea name="%2$s" id="%1$s" %5$s>%3$s</textarea></p>', array(
                isset( $args['id'] ) ? esc_attr( $args['id'] ) : esc_attr( $args['name'] ),
                esc_attr( $args['name'] ),
                esc_attr( $args['value'] ),
                isset( $args['label'] ) ? esc_html( $args['label'] ) : '',
                isset( $args['required'] ) && $args['required'] ? 'required' : '',
                isset( $args['required'] ) && $args['required'] ? ' <span class="required-field">*</span>' : '',
            ) );
            break;
        default:
            return vsprintf( '<p><label for="%1$s">%5$s%7$s</label><input type="%3$s" name="%2$s" id="%1$s" value="%4$s" %6$s></p>', array(
                isset( $args['id'] ) ? esc_attr( $args['id'] ) : esc_attr( $args['name'] ),
                ! empty( $args['name'] ) ? esc_attr( $args['name'] ) : '',
                isset( $args['type'] ) ? esc_attr( $args['type'] ) : 'text',
                ! empty( $args['value'] ) ? esc_attr( $args['value'] ) : '',
                isset( $args['label'] ) ? esc_html( $args['label'] ) : '',
                isset( $args['required'] ) && $args['required'] ? 'required' : '',
                isset( $args['required'] ) && $args['required'] ? ' <span class="required-field">*</span>' : '',
            ) );
    }
}

/**
 * Formulaire qui sert à la fois à la création et à l’update d’un utilisateur
 */
function w_demo_form( $exist = false ) {
	
    $user = $exist ? wp_get_current_user() : false; // ais-je besoin d'infos existantes ?
    $action = $exist ? 'edit' : 'create'; // création ou update ?

    $form_wrapper = '<form method="post" action="' . admin_url( 'admin-post.php' ) . '" class="w-demo-form">%s</form>';
	// Cette entrée « action » permet de router admin-post.php vers une fonction précise
    $out = array( '<input type="hidden" name="action" value="' . $action . '-customuser">' );
	// On créer un nonce qui nous permetrta de vérifier que l’utilisateur est bien à l’origine de l’action
    $out[] = wp_nonce_field( "{$action}-user-" . ( $exist ? $user->ID : $_SERVER['REMOTE_ADDR'] ), "nonce-{$action}", false, false );

    $out[] = w_demo_field( array( // Prénom
        'name'     => 'user_firstname', 
        'value'    => $exist ? $user->user_firstname : '', 
        'label'    => __( 'Votre prénom', 'w-profil-membre' ),
        'required' => true,
    ) );
    
    $out[] = w_demo_field( array( // Nom
        'name'     => 'user_lastname', 
        'value'    => $exist ? $user->user_lastname : '', 
        'label'    => __( 'Votre nom', 'w-profil-membre' ),
        'required' => true,
    ) );

    $out[] = w_demo_field( array( // Email
        'name'     => 'user_email', 
        'value'    => $exist ? $user->user_email : '', 
        'type'     => 'email',
        'label'    => __( 'Votre adresse email', 'w-profil-membre' ),
        'required' => true,
    ) );

    $out[] = w_demo_field( array( // Mot de passe
        'name'     => 'user_pass', 
        'type'     => 'password',
        'label'    => __( 'Votre mot de passe', 'w-profil-membre' ),
        'required' => $exist ? false : true,
    ) );
	
    $out[] = '<button type="submit">' . __( 'Enregistrer le profil', 'w-profil-membre' ) . '</button>';

    return sprintf( $form_wrapper, implode( PHP_EOL, $out ) );
}

/**
 * Met à jour un utilisateur
 *
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/admin_post_(action)
 * @see https://codex.wordpress.org/Function_Reference/check_admin_referer
 * @see https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 * @see https://codex.wordpress.org/Function_Reference/wp_update_user
 */
add_action( 'admin_post_edit-customuser', 'w_demo_update_user' );
function w_demo_update_user() {
	// Je vérifie le nonce
    check_admin_referer( 'edit-user-' . get_current_user_id(), 'nonce-edit' );

	// Je créer son display_name
    $nom = implode( ' ', array_filter( array(
        ucfirst( sanitize_text_field( $_POST['user_firstname'] ) ),
        ! empty( $_POST['user_lastname'] ) ? ucfirst( sanitize_text_field( $_POST['user_lastname'] ) ) : false,
    ) ) );

    // Mise à jour des champs du donateur
    $infos = array(
        'ID'           => get_current_user_id(),
        'first_name'   => sanitize_text_field( $_POST['user_firstname'] ),
        'last_name'    => sanitize_text_field( $_POST['user_lastname'] ),
        'user_email'   => $_POST['user_email'],
        'display_name' => $nom,
    );
    $id = wp_update_user( $infos );

    if ( is_wp_error( $id ) ) {
        return false;
    }

	// Mise à jour du mot de passe de l’utilisateur
    if ( ! empty( $_POST['user_pass'] ) ) {
        wp_set_password( $_POST['user_pass'], $id );
        $user = new WP_User( $id );
        wp_set_auth_cookie( $user->ID );
        wp_set_current_user( $user->ID );
        do_action( 'wp_login', $user->user_login, $user );
    }

    wp_redirect( get_permalink( get_option( 'profil-page' ) ), 301 );
	exit;
}

/**
 * Créer un nouvel utilisateur
 *
 * @see https://developer.wordpress.org/reference/hooks/admin_post_nopriv_action/
 * @see https://codex.wordpress.org/Function_Reference/check_admin_referer
 * @see https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
 * @see https://codex.wordpress.org/Function_Reference/wp_insert_user
 * @see https://codex.wordpress.org/Function_Reference/wp_signon
 */
add_action( 'admin_post_nopriv_create-customuser', 'w_demo_create_user' );
function w_demo_create_user( $args, $update = false ) {
	// Je vérifie le nonce
    check_admin_referer( 'create-user-' . $_SERVER['REMOTE_ADDR'], 'nonce-create' );

	// Je créer son display_name
    $nom = implode( ' ', array_filter( array(
        ucfirst( sanitize_text_field( $_POST['user_firstname'] ) ),
        ! empty( $_POST['user_lastname'] ) ? ucfirst( sanitize_text_field( $_POST['user_lastname'] ) ) : false,
    ) ) );

    // Création de l’utilisateur
    $infos = array(
        'first_name'   => sanitize_text_field( $_POST['user_firstname'] ),
        'last_name'    => sanitize_text_field( $_POST['user_lastname'] ),
        'user_login'   => $_POST['user_email'], // Le login est son email :-)
        'user_email'   => $_POST['user_email'],
        'user_pass'    => $_POST['user_pass'],
        'role'         => 'member',
        'display_name' => $nom,
    );
    $id = wp_insert_user( $infos );

    if ( is_wp_error( $id ) ) {
        return false;
    }

	// Je log automatiquement l’utilisateur
    $creds = array();
    $creds['user_login']    = $_POST['user_email'];
    $creds['user_password'] = $_POST['user_pass'];
    $creds['remember']      = true;
    $user = wp_signon( $creds, false );

    wp_redirect( get_permalink( get_option( 'profil-page' ) ), 301 );
	exit;
}

/**
 * Ne pas afficher l’admin bar pour les simples membres
 *
 * @see https://codex.wordpress.org/Function_Reference/show_admin_bar
 */
add_filter( 'show_admin_bar', 'w_demo_show_admin_bar' );
function w_demo_show_admin_bar( $show ) {
	if ( current_user_can( 'membre' ) ) {
		$show = false;
	}
	return $show;
}
