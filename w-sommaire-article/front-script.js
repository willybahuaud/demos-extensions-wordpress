jQuery( document ).ready(function( $ ) {
    // Si un sommaire existe
	if( $( '.sommaire-wrapper' ).length ) {
        $(document).on('click', '#sommaire-wrapper a', function(e) {
            e.preventDefault();
            // trouver l’ancre
            var anchor = $(this).attr('href');
            // Se déplacer en smooth scroll vers cette ancre
            $('html,body').animate({
                scrollTop: $(anchor).offset().top - 100 // -100px pour éviter de coller le titre en haut de l’écran
            }, 300);
        });
    }
});
