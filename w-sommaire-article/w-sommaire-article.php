<?php

/**
 * Plugin name: Sommaire d’article
 * Plugin URI: https://plugin.example.com
 * Description: une extension de sommaire d’article
 * Author : toi
 * Author URI: https://example.com
 * Contributors: toi, moi
 * Text Domain: w-sommaire-article
 * Version: 0.1
 * Stable tag: 0.1
 */

define( 'W_DEMO_SOMM_VER', 0.1 );

/**
 * Bloquer les accès directs
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html__( 'Cheatin&#8217; uh?' ) );
}

/**
 * Charger les ressources
 * 
 * @see https://developer.wordpress.org/reference/functions/wp_register_script/
 */
add_action( 'wp_enqueue_scripts', 'w_demo_sommaire_register_assets' );
function w_demo_sommaire_register_assets() {
    wp_register_script( 'w-demo-sommaire-front-script', plugins_url( 'front-script.js', __FILE__ ), array( 'jquery' ), W_DEMO_SOMM_VER, true );
}

/**
 * Appeler le sommaire (ou le générer s’il n’existe pas)
 *
 * @see https://developer.wordpress.org/reference/functions/get_post_meta/
 * @use w_demo_sommaire_save_menu
 */
function w_demo_sommaire_get_menu() {
	$obj = get_queried_object();
	if ( in_array( $obj->post_type, array( 'page', 'post' ) ) ) {
		if ( false === $sommaire = get_post_meta( $obj->ID, 'sommaire', true ) ) {
			// Je trouve/génère le sommaire
			$sommaire = w_demo_sommaire_save_menu( $obj->ID, $obj, true );
		}
		// J’appelle le script
		wp_enqueue_script( 'w-demo-sommaire-front-script' );
	}
	return $sommaire;
}

/**
 * Génère le menu lors de la sauvegarde d’une page ou d’un article
 * 
 * @see https://developer.wordpress.org/reference/hooks/save_post/
 * @see https://developer.wordpress.org/reference/functions/sanitize_title/
 * @use https://www.regex101.com/ pour créer une expression rationnelle
 * @use w_demo_sommaire_replace_title_content pour supprimer les ID des titres
 */
add_action( 'save_post', 'w_demo_sommaire_save_menu', 10, 3 );
function w_demo_sommaire_save_menu( $post_id, $post, $update ) {
	if ( ! $update // S’il ne s’agit pas d’une mise à jour…
	  || ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) // … ou ’il s’agit d’une sauvegarde automatique …
	  || wp_is_post_revision( $post_id ) // … ou ’il s’agit d’une révision …
	  || ! in_array( $post->post_type, array( 'post', 'page' ) ) // … ou encore d’un contenu autre qu’une page ou un article
	  ) {
		// Alors on ne fait rien !
		return;
	}

	// On recherche tous les titres de niveau 2, 3 ou 4 dans le post_content
	$original_content = $post->post_content;
	$pattern = '/<h([2-3])(.*)>(.*)<\/h\1>/iU';
	// On supprime les attributs id existants dans ces titres
	$original_content = preg_replace_callback( $pattern, 'w_demo_sommaire_replace_title_content', $original_content );
	// On trouve tous ces titres
	preg_match_all( $pattern, $original_content, $results );

	// S’il y a des titres de niveau 2, 3 ou 4
	if ( ! empty( $results[0] ) ) {

		// On construit le sommaire
		$lvl2 = $lvl3 = $lvl4 = 0;
		$obj = '<nav id="sommaire-wrapper" class="sommaire-wrapper">
		<span class="titre">' . __( 'Sommaire', 'w-sommaire-article' ) . '</span><ol>';

		// On défini l’apparence de chaque titre selon son niveau
		foreach ( $results[3] as $k => $titre ) {
			switch( $results[1][ $k ] ) {
				case 2:
					// les titres de niveau 2 auront la forme `1 • Titre`
					$niveau = '<span class="title_lvl">' . ++$lvl2 . ' • </span>';
					$lvl3 = 0;
					$lvl4 = 0;
					break;

				case 3:
					// les titres de niveau 3 auront la forme `1.2 • Titre`
					$niveau = '<span class="title_lvl">' . $lvl2 . '.' . ++$lvl3 . ' • </span>';
					$lvl4 = 0;
					break;

				case 4:
					// les titres de niveau 3 auront la forme `a) Titre`
					$niveau = '<span class="title_lvl">' . chr( ++$lvl4 + 65 ) . ')</span>';
					break;
			}

			// on écrit l’entrée du menu
			$lvl = $results[1][ $k ];
			$ancre = sanitize_title( $titre );
			$obj .= "<li class=\"level{$lvl}\"><a href=\"#{$ancre}\" class=\"title_lvl{$lvl}\">{$niveau}{$titre}</a></li>";
		}

		$obj .= '</ol></nav>';

	} else {
		// Sinon on retourne un objet vide
		$obj = '';
	}

	// On supprime notre hook le temps de la sauvegarde
	remove_action( 'save_post', 'w_demo_sommaire_save_menu', 10, 3 );
	// On met à jour le contenu avec nos belles ancres
	wp_update_post( array( 'ID' => $post_id, 'post_content' => $original_content ) );
	// On réactive notre hook
	add_action( 'save_post', 'w_demo_sommaire_save_menu', 10, 3 );
	// On sauvegarde le sommaire dans une meta
	update_post_meta( $post_id, 'sommaire', $obj );

	// On renvoi le sommaire à l’affichage
	return $obj;
}

/**
 * Supprime les ID des titres trouvés par la regex
 */
function w_demo_sommaire_replace_title_content( $matches ) {
	// Supprimer l’attribut id des titres
	$atts = preg_replace( '/id=\"([^\"])*\"/', '', $matches[2] );
	// renvoyer le titre avec un attribut id correspondant
	$balise_lvl = $matches[1];
	$attributs  = trim( $atts );
	$titre_id   = sanitize_title( $matches[3] );
	$titre      = $matches[3];
	return "<h{$balise_lvl} id=\"{$titre_id}\" {$attributs}>{$titre}</h{$balise_lvl}>";
}

/**
 * Affiche le sommaire au début de l’article
 *
 * @see https://developer.wordpress.org/reference/hooks/the_content/
 * @see https://developer.wordpress.org/reference/functions/is_singular/
 * @see https://codex.wordpress.org/Conditional_Tags
 */
add_filter( 'the_content', 'w_demo_show_menu', 11 );
function w_demo_show_menu( $content ) {
	if ( is_singular( array( 'post', 'page' ) ) ) {
		$content = w_demo_sommaire_get_menu() . $content;
	}
	return $content;
}
