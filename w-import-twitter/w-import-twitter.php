<?php

/**
 * Plugin name: Importeur de tweets
 * Plugin URI: https://plugin.example.com
 * Description: une extension pour importer des tweets
 * Author : toi
 * Author URI: https://example.com
 * Contributors: toi, moi
 * Text Domain: w-import-twitter
 * Version: 0.1
 * Stable tag: 0.1
 */

/**
 * Bloquer les accès directs
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html__( 'Cheatin&#8217; uh?' ) );
}

/**
 * Avant tout : on charge la classe qui gère les importeurs WordPress
 *
 * @use https://developer.wordpress.org/reference/classes/wp_importer/
 */
require_once ABSPATH . 'wp-admin/includes/import.php';
if ( ! class_exists( 'WP_Importer' ) ) {
	$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
	if ( file_exists( $class_wp_importer ) ) {
		require_once $class_wp_importer;
	}
}

/**
 * On enregistrer notre propre importeur
 *
 * @see https://developer.wordpress.org/reference/functions/register_importer/
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/admin_post_(action)
 * @use w_demo_import_tweets_get_tweets() pour récupérer les posts puis faire l’insertion
 */
register_importer( 'w-twitter-feed', 'Hashtag Twitter feed', __( 'Importer les tweets', 'w-import-twitter' ), 'w_demo_import_tweets_panel' );
function w_demo_import_tweets_panel() {
    ?>
    <div class="wrap">
    <h2><?php _e( 'Importer les tweets en fonction du hashtag', 'w-import-twitter' ); ?></h2>
    <form action="<?php echo admin_url( 'admin-post.php' ); ?>">
    <input type="hidden" name="action" value="tweets.get">
    <table class="form-table">
    <tr>
        <th><label for="rechercher"><?php _e( 'Phrase de recherche', 'w-import-twitter' ); ?></label></th>
        <td><input type="text" value="#wpnantes" name="recherche"></td>
    </tr>
    <tr>
        <th><label for="number"><?php _e( 'Nombre de tweets à importer', 'w-import-twitter' ); ?></label></th>
        <td><input type="number" name="number" value="5"></td>
    </tr>
    
    <?php wp_nonce_field( "import-tweets-{$_SERVER['REMOTE_ADDR']}-" . get_current_user_id(), '_import-tweets-nonce' ); ?>
    
    </table>
    <input type="submit" value="<?php _e( 'Importer !', 'w-import-twitter' ); ?>" class="button button-primary">
    </form>
    </div>
    <?php
}

/**
 * On récupère les tweets qui nous intéressent
 *
 * @see https://github.com/TweetPressFr/TokenToMe la class qu’on utilise pour récuperer les tweets
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/admin_post_(action)
 */
add_action( 'admin_post_tweets.get', 'w_demo_import_tweets_get_tweets' );
function w_demo_import_tweets_get_tweets() {
    // On vérifie que l’utilisateur est bien à l’origine de la demande
    check_admin_referer( "import-tweets-{$_SERVER['REMOTE_ADDR']}-" . get_current_user_id(), '_import-tweets-nonce' );
    // On charge la classe dont le rôle est d‘interroger Twitter
    require_once( 'classes/TokenToMe.class.php' );
	// Ok pour utiliser ma clé pour la démo…
    $consumer_key = 'cw5Sz1FvoHgezRWpsXSxurRFY';
    $consumer_secret = 'S8ePAid3P1lKZ3Zn0NqCVq0Q9RioVWPykMa0SsYxYMXCpqszPI';

    // On construit notre requête
    $q     = $_GET['recherche'];
    $count = $_GET['number'];
    $since = false;
    $query = array(
			'q'             => urlencode( $q ),
			'result_type'   => 'recent',
			'count'         => $count,
			'since_id'      => $since,
            'lang'          => 'fr',
		);
    $init = new TokenToMe\WP_Twitter_Oauth(
		$consumer_key, // clé privée
		$consumer_secret, // clé secrete
		'search/tweets', // méthode
		$query, // requête
		60
	);

    // On demande les tweets
    $infos = $init->get_infos(); // ne pas hésiter à faire un var_dump ici pour voir ce que l’on obtient !
    if ( ! empty( $infos->statuses ) ) {
        $posts = [];

        // Pour chaque tweet, on organise nos
        foreach ( $infos->statuses as $status ) {
            $post    = [];
            $content = $status->text;
            // cette variable nous permettra de stocker les chaines à supprimer dans le contenu du tweet
            $to_del  = []; 
            // On formatte la date pour WordPress
            $post['post']['post_date']     = date( 'c', strtotime( $status->created_at ) );
            // On enregistre l’ID du tweet sur twitter, pour éviter de l’importer plusieurs fois
            $post['post']['meta']['idExt'] = $status->id;
            // Pousser chaque média dans la variable
            if ( isset( $status->entities->media ) ) {
                foreach ( $status->entities->media as $media ) {
                    $post['post']['media'][] = [ 'url' => $media->media_url, 'idExt' => $media->id ];
                    $to_del[] = $media->indices;
                }
            }
            // Pousser chaque hashtag dans une catégorie
            if ( isset( $status->entities->hashtags ) ) {
                foreach ( $status->entities->hashtags as $hash ) {
                    $post['taxo']['category'][] = ['name' => $hash->text, 'indices' => $hash->indices ];
                }
                $post['taxo']['category'] = array_reverse( $post['taxo']['category'] );
            }

            // supprimer les liens vers les medias
            if ( ! empty( $to_del ) ) {
                array_multisort( $to_del );
                // Il faut retourner le tableaux pour supprimer par la fin, sinon les indexes des caractères vont changer ;-)
                $to_del = array_reverse( $to_del );
                foreach ( $to_del as $pos ) {
                    $content = substr_replace( $content, '', $pos[0], $pos[1] - $pos[0] + 1 );
                }
            }
            $post['post']['post_title'] = sprintf( __( 'Tweet par %s à %s', 'w-import-twitter' ), $status->user->name, date( 'H:i', strtotime( $status->created_at ) ) );
            $post['post']['post_content'] = $content;
            $posts[] = $post;
        }
        // J’applique une fonction pour procéder à l’insertion en BDD
        array_map( 'w_demo_import_tweets_import_content', $posts );
    }
    // Quand c’est fini, rediriger vers les posts insérés
    wp_redirect( admin_url( 'edit.php' ), 301 );
    exit();
}

/**
 * Procéder à l’enregistrement des contenus dans WordPress
 *
 * @see https://developer.wordpress.org/reference/functions/wp_insert_term/
 * @see https://developer.wordpress.org/reference/functions/wp_set_object_terms/
 * @see https://developer.wordpress.org/reference/functions/wp_insert_post/
 * @use get_tweet_content_by_idExt()
 */
function w_demo_import_tweets_import_content( $post ) {

	// On importe les hashtag sous forme de catégorie
    $terms = $post['taxo']['category'];
    $all_terms = [];
    if ( ! empty( $terms ) ) {
        foreach ( $terms as $term ) {
            if ( ! $t = term_exists( $term['name'], 'category' ) ) {
            $t = wp_insert_term( $term['name'], 'category' );
            }
            $all_terms[] = $t['term_id']; 
        }
    }

    // On importe le contenu du tweet (si celui-ci n’a pas déjà été importé)
    if ( ! get_tweet_content_by_idExt( $post['post']['meta']['idExt'] ) ) {
        $meta = ! empty( $post['post']['meta'] ) ? $post['post']['meta'] : [];
        $media = ! empty( $post['post']['media'] ) ? $post['post']['media'] : [];
        $post['post']['post_type']   = 'post';
        $post['post']['post_status'] = 'publish';
        $tweet = wp_insert_post( $post['post'], true );
        // Essayer d’importer chaque media
        foreach ( $media as $k => $m ) {
            // Regarder si le media à déjà été importé, sinon essayer de l’importer
            if ( ! $media_id = get_tweet_content_by_idExt( $m['idExt'] ) ) {
                $media_id = w_demo_import_tweets_import_image( $m['url'], $tweet );
            }
            // Si l’image à été importée, la mettre en image à la une
            if ( ! is_wp_error( $media_id ) && 0 === $k ) {
                $meta['_thumbnail_id'] = $media_id;
            }
        }

        // On associes les hastags (catégories) au tweet fraichement importé
        wp_set_object_terms( $tweet, array_map( 'intval', $all_terms ), 'category' );

        // Enfin on enregistre tous les media
        foreach ( $meta as $k => $m ) {
            update_post_meta( $tweet, $k, $m );
        }
    }
}

/**
 * Permet de retrouver  un tweet déjà importé, grâce à son ID externe
 *
 * @see https://codex.wordpress.org/Class_Reference/WP_Query
 * @see https://developer.wordpress.org/reference/functions/get_posts/
 */
function get_tweet_content_by_idExt( $idExt ) {
    $posts = get_posts( array(
        'posts_per_page'   => 1,
        'post_type'        => 'any',
        'suppress_filters' => false,
        'no_found_rows'    => 1,
        'meta_query'       => array( array(
            'key'          => 'idExt',
            'value'        => $idExt,
        ) ),
    ) );
    if ( ! empty( $posts ) ) {
        return $posts[0]->ID;
    }
    return false;
}

/**
 * Permet de récupérer et d’importer les images
 *
 * @see https://developer.wordpress.org/reference/functions/media_handle_sideload/
 * @see https://markwilkinson.me/2015/07/using-the-media-handle-sideload-function/
 */
function w_demo_import_tweets_import_image( $url, $idExt = '', $post_id = 0 ) {

    // On charge les dépendances de la fonction (qui sont côté back)
    if ( ! function_exists( 'media_handle_upload' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
    }

    // On télécharge le fichier
	$tmp = download_url( $url );
	if ( is_wp_error( $tmp ) ) {
		return false;
	}
	$file_array = array();

	preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png|pdf)/i', $url, $matches );
	$file_array['name'] = basename( $matches[0] );
	$file_array['tmp_name'] = $tmp;

	if ( is_wp_error( $tmp ) ) {
		@unlink($file_array['tmp_name']);
		$file_array['tmp_name'] = '';
	}

    // On indique à WordPress qu’un média doit être « pris en compte »
	$id = media_handle_sideload( $file_array, $post_id );

	if ( is_wp_error($id) ) {
		@unlink($file_array['tmp_name']);
		return $id;
	}
    // On met à jour l’ID externe du media
	update_post_meta( $id, 'idExt', $idExt );

    // On renvoie l’ID du media
	return $id;
}
