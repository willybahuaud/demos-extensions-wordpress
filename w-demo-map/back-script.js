jQuery( document ).ready(function( $ ) {
    // si on est dans le back office
	if( $( '#place-pointer' ).length > 0 ) {
        // Initialization de la map
		var map = L.map( 'place-pointer' ).setView( [ currentCoords['lat'], currentCoords['lng'] ], 13);
		L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo( map );
        // on place le curseur sur les valeurs sauvegardées
		var fmarker = L.marker([ currentCoords['lat'], currentCoords['lng'] ], {
			draggable:true
		} ).addTo( map );

        // lorsque l’on déplace le marker, on met à jour les coordonnées
		fmarker.on( 'dragend', function(event){
			var marker = event.target;
			var position = marker.getLatLng();
			map.setView( position, map.getZoom() );
			var coords = position.lat.toFixed(7) + ',' + position.lng.toFixed(7);
			$( '#geo-coords' ).val( coords );
		} );
	
        // lorsque je valide une adresse (dans le champ texte), on récupère les coordonnées GPS
		$( '#geo-search' ).on( 'keydown', function( e ) {
			if ( e.keyCode === 13 ) { // apuie sur « entrée »
				e.preventDefault();
				$.ajax({
					method: 'GET',
					url: 'https://maps.googleapis.com/maps/api/geocode/json',
					data: {
						address: $(this).val(),
					},
					success: function (data) {
						if ( typeof data.results[0].geometry.location != 'undefined' ) {
							var coords = data.results[0].geometry.location;
							var newLatLng = new L.LatLng( coords.lat, coords.lng );
							fmarker.setLatLng( newLatLng );
							map.setView( coords, map.getZoom() );
							coords = coords.lat.toFixed(7) + ',' + coords.lng.toFixed(7);
							$( '#geo-coords' ).val( coords );
						}
					}
				});
			}
		});
	}
});
