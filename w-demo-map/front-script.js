jQuery( document ).ready(function( $ ) {
	if( $( '.map' ).length ) {
        // On appelle toutes les fiches avec une promise
        var listComposter = $.ajax( fichesUrl, {
            method:'GET',
            dataType:'json',
        });

        // Pour chaque carte
        $( '.map' ).each(function(){
            $self = $(this);
            $self.height(450);

            // On charge une map
            var map = L.map( $self.attr('id'), {
                scrollWheelZoom:false,
            } ).setView( [0, 0], 4);
            // voir la liste des maps ici : http://www.creazo.fr/listing-des-fonds-de-cartes-dopen-street-map/
            L.tileLayer( 'https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png' ).addTo( map );

            // Quand la promise est résolue…
            listComposter.done(function(data){
                markers = [];
                $.each(data,function(i,el){
                    if ( typeof el.localisation != 'undefined' 
                      && el.localisation ) {
                        // … on ajoute les points 
                        var marker = L.marker([ el.localisation.lat, el.localisation.lng ], {
                            // on peut ajouter des options ici
                        } )
                        .bindPopup( formatPopup(el) )
                        .addTo(map);
                        markers.push( marker );
                    }
                });
                // On recentre la caret sur les points
                var group = new L.featureGroup( markers );
                map.fitBounds( group.getBounds() );
            });
        })
    }

    function formatPopup( el ) {
        return '<a href="' + el.link + '">'
        + el.title.rendered
        + '</a>';
        return el.title.rendered;
    }
});

