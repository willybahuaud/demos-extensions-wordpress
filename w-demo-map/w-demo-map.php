<?php

/**
 * Plugin name: demo map
 * Plugin URI: https://plugin.example.com
 * Description: une extension de carte
 * Author : toi
 * Author URI: https://example.com
 * Contributors: toi, moi
 * Text Domain: w-demo-map
 * Version: 0.1
 * Stable tag: 0.1
 */

define( 'W_DEMO_VER', 0.1 );

/**
 * Bloquer les accès directs
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html__( 'Cheatin&#8217; uh?' ) );
}

/**
 * Charger les ressources
 * 
 * @see https://developer.wordpress.org/reference/functions/wp_register_script/
 * @see https://developer.wordpress.org/reference/functions/wp_register_style/
 */
add_action( 'wp_enqueue_scripts', 'w_demo_map_register_assets' );
add_action( 'admin_enqueue_scripts', 'w_demo_map_register_assets' );
function w_demo_map_register_assets() {
	wp_register_style( 'leaflet-css', 'https://unpkg.com/leaflet@0.7.7/dist/leaflet.css', array(), W_DEMO_VER, 'all' );
	wp_register_script( 'leaflet', 'https://unpkg.com/leaflet@0.7.7/dist/leaflet.js', array(), W_DEMO_VER, true );
    wp_register_script( 'w-demo-front-script', plugins_url( 'front-script.js', __FILE__ ), array( 'leaflet', 'jquery' ), true );
	wp_register_script( 'w-demo-back-script', plugins_url( 'back-script.js', __FILE__ ), array( 'leaflet', 'jquery' ), W_DEMO_VER, true );
}

/**
 * Déclare le type de contenu
 *
 * @see https://developer.wordpress.org/reference/functions/register_post_type/
 * @see https://generatewp.com/post-type/
 * @see https://developer.wordpress.org/resource/dashicons/
 */
add_action( 'init', 'w_demo_map_register_fiches' );
function w_demo_map_register_fiches() {
    register_post_type( 'fiche', array(
        'label'         => __( 'Fiches', 'w-demo-map' ),
		'public'        => true,
		'supports'      => array( 'title', 'thumbnail', 'editor' ),
		'has_archive'   => false,
		'menu_icon'     => 'dashicons-location-alt',
		'menu_position' => 21,
		'show_in_rest'  => true,
    ) );
}

/**
 * Metabox pour les coordonnées GPS
 *
 * @see https://wabeo.fr/metabox-geolocalisation-v2/ (tuto)
 * @see 1• https://developer.wordpress.org/reference/functions/add_meta_box/ 
 * … ou https://developer.wordpress.org/reference/hooks/add_meta_boxes_post_type/
 */

// 1• appeler la box
add_action( 'add_meta_boxes_fiche', 'w_demo_map_load_metabox', 10, 2 );
function w_demo_map_load_metabox( $post ) {
    add_meta_box( 'w_demo_map-geo-coords', __( 'Coordonnées', 'w-demo-map' ), 'w_demo_map_geo_metabox', $post->post_type, 'side', 'core' );
}

// 2• générer la box
function w_demo_map_geo_metabox( $post ) {
	// Récupérer les hypotétiques coordonnées de la fiche
	$geo_lat  = get_post_meta( $post->ID, 'lat', true );
	$geo_lng  = get_post_meta( $post->ID, 'lng', true );
	$map = array(
	        'lat' => $geo_lat ? floatval( $geo_lat ) : 47.09,
	        'lng' => $geo_lng ? floatval( $geo_lng ) : -1.39 );

    // Appeler les assets back
	wp_enqueue_style( 'leaflet-css' );
	wp_enqueue_script( 'w-demo-back-script' );

	// Et pousser les variables javascript nécessaires
	wp_localize_script( 'w-demo-back-script', 'currentCoords', $map );

	// Écrire la box
	echo '<input type="search" id="geo-search" placeholder="' . __( 'Adresse...', 'w-demo-map' ) . '" class="widefat">';
	echo '<div id="place-pointer" class="place-pointer" style="height:250px;width:100%;"></div>';

	$coords = $geo_lng && $geo_lat ? implode( ',', array( $geo_lat, $geo_lng ) ) : '';
	echo '<input type="text" id="geo-coords" name="geo-coords" value="' . esc_attr( $coords ) . '" class="widefat">';

	// Nonce pour la sécurité
	wp_nonce_field( "geo-save_{$post->ID}", 'geo-nonce') ;
}

// 3• sauvegarder la box
add_action( 'save_post_fiche', 'w_demo_map_save_metabox' );
function w_demo_map_save_metabox( $post ) {
	// On ne fait rien en cas d’autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( isset( $_POST['geo-coords'] ) && trim( $_POST['geo-coords'] ) != '' ) {
		// On vérifie le nonce
		check_admin_referer( "geo-save_{$_POST['post_ID']}", 'geo-nonce');

		// On sauvegarde
		$coords = array_map( 'trim', explode( ',', $_POST['geo-coords'] ) );
		update_post_meta( $_POST['post_ID'], 'lat', floatval( $coords[0] ) );
		update_post_meta( $_POST['post_ID'], 'lng', floatval( $coords[1] ) );
	}
}

/**
 * Shortcode + fonction d’affichage
 *
 * @see https://developer.wordpress.org/reference/functions/add_shortcode/
 * @see https://developer.wordpress.org/reference/functions/wp_localize_script/
 */
add_shortcode( 'w-demo-map', 'w_demo_map_render' );
function w_demo_map_render( $atts, $content = '' ) {
    // On appelle nos assets
    wp_enqueue_style( 'leaflet-css' );
    wp_enqueue_script( 'w-demo-front-script' );
    // et on pousse les variables javascript requises
    wp_localize_script( 'w-demo-front-script', 'fichesUrl', home_url( '/wp-json/wp/v2/fiche?per_page=100' ) );

    // et on retourne le markup de la carte
	$map_id = 'map-' . uniqid();
    return '<div class="map" id="' . $map_id . '"></div>';
}

/**
 * API Rest Route
 *
 * @see https://developer.wordpress.org/reference/functions/register_rest_field/
 * @todo penser à flusher les permaliens
 */
add_action( 'rest_api_init', 'w_demo_map_add_rest_field' );
function w_demo_map_add_rest_field() {
    // On ajoute à la WP API les coordonnées GPS
    register_rest_field( 'fiche', 'localisation', array(
        'get_callback' => function( $obj ) {
            $lat = get_post_meta( $obj['id'], 'lat', true );
            $lng = get_post_meta( $obj['id'], 'lng', true );
            return $lat && $lng ? compact( 'lat', 'lng' ) : '';
        },
        'schema' => array(
            'description' => __( 'Localisation de la fiche', 'w-demo-map' ),
        ),
    ) );
}
