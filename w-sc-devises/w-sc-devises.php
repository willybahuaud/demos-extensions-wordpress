<?php

/**
 * Plugin name: Shortcode de conversion de devises
 * Plugin URI: https://plugin.example.com
 * Description: une extension pour convertir des devises
 * Author : toi
 * Author URI: https://example.com
 * Contributors: toi, moi
 * Text Domain: w-sc-devises
 * Version: 0.1
 * Stable tag: 0.1
 */

/**
 * Bloquer les accès directs
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( esc_html__( 'Cheatin&#8217; uh?' ) );
}

/**
 * Obtenir le taux de change
 *
 * @see https://stackoverflow.com/questions/3139879/how-do-i-get-currency-exchange-rates-via-an-api-such-as-google-finance (choix des api)
 * @see http://fixer.io/ (l’api financière utilisée)
 */
function w_demo_get_taux_currencies( $from, $to ) {
	if ( ! $taux_from = get_transient( "taux-{$from}" ) ) {
		$req = wp_remote_get( "http://api.fixer.io/latest?base={$from}" );
		if ( is_wp_error( $req )
		  || 200 !== wp_remote_retrieve_response_code( $req ) ) {
			return false;
		}
		$body = wp_remote_retrieve_body( $req );
		$data = json_decode( $body );
		if ( empty( $data->rates ) ) {
			return false;
		}
		$taux_from = $data->rates;
		set_transient( "taux-{$from}", $taux_from, DAY_IN_SECONDS );
	}
	if ( ! empty( $taux_from->$to ) ) {
		return $taux_from->$to;
	}
	return false;
}

/**
 * Enregistrer le shortcode
 *
 * Exemple du shortcode : `[conversion valeur="2.2" de="EUR" vers="JPY" /]`
 *
 * @see https://developer.wordpress.org/reference/functions/add_shortcode/
 * @see https://developer.wordpress.org/reference/functions/shortcode_atts/
 * @use w_demo_get_taux_currencies() pour obtenir le taux de change
 * @use w_demo_get_symbol() pour obtenir le symbole de la devise de sortie
 */
add_shortcode( 'conversion', 'wabeo_conversion' );
function wabeo_conversion( $atts, $content = '', $tag ) {
	$atts = shortcode_atts( array(
	    	'valeur' => false,
	    	'de'     => 'EUR',
	    	'vers'   => 'GBP',
	        ), $atts, $tag );
	if ( $taux = w_demo_get_taux_currencies( $atts['de'], $atts['vers'] ) ) {
		$value  = floatval( $atts['valeur'] ) * $taux;
		$symbol = w_demo_get_symbol( $atts['vers'] );
		return number_format_i18n( $value, 2 ) . '&nbsp;' . $symbol;
	}
}

/**
 * Retourne le symbole de la devise (pompé sur Woocommerce)
 *
 * @see https://docs.woocommerce.com/wc-apidocs/source-function-get_woocommerce_currency_symbol.html#445-624
 */
function w_demo_get_symbol( $to ) {
	$symbol = array(
		'AED' => '&#x62f;.&#x625;',
        'AFN' => '&#x60b;',
        'ALL' => 'L',
        'AMD' => 'AMD',
        'ANG' => '&fnof;',
        'AOA' => 'Kz',
        'ARS' => '&#36;',
        'AUD' => '&#36;',
        'AWG' => '&fnof;',
        'AZN' => 'AZN',
        'BAM' => 'KM',
        'BBD' => '&#36;',
        'BDT' => '&#2547;&nbsp;',
        'BGN' => '&#1083;&#1074;.',
        'BHD' => '.&#x62f;.&#x628;',
        'BIF' => 'Fr',
        'BMD' => '&#36;',
        'BND' => '&#36;',
        'BOB' => 'Bs.',
        'BRL' => '&#82;&#36;',
        'BSD' => '&#36;',
        'BTC' => '&#3647;',
        'BTN' => 'Nu.',
        'BWP' => 'P',
        'BYR' => 'Br',
        'BZD' => '&#36;',
        'CAD' => '&#36;',
        'CDF' => 'Fr',
        'CHF' => '&#67;&#72;&#70;',
        'CLP' => '&#36;',
        'CNY' => '&yen;',
        'COP' => '&#36;',
        'CRC' => '&#x20a1;',
        'CUC' => '&#36;',
        'CUP' => '&#36;',
        'CVE' => '&#36;',
        'CZK' => '&#75;&#269;',
        'DJF' => 'Fr',
        'DKK' => 'DKK',
        'DOP' => 'RD&#36;',
        'DZD' => '&#x62f;.&#x62c;',
        'EGP' => 'EGP',
        'ERN' => 'Nfk',
        'ETB' => 'Br',
        'EUR' => '&euro;',
        'FJD' => '&#36;',
        'FKP' => '&pound;',
        'GBP' => '&pound;',
        'GEL' => '&#x10da;',
        'GGP' => '&pound;',
        'GHS' => '&#x20b5;',
        'GIP' => '&pound;',
        'GMD' => 'D',
        'GNF' => 'Fr',
        'GTQ' => 'Q',
        'GYD' => '&#36;',
        'HKD' => '&#36;',
        'HNL' => 'L',
        'HRK' => 'Kn',
        'HTG' => 'G',
        'HUF' => '&#70;&#116;',
        'IDR' => 'Rp',
        'ILS' => '&#8362;',
        'IMP' => '&pound;',
        'INR' => '&#8377;',
        'IQD' => '&#x639;.&#x62f;',
        'IRR' => '&#xfdfc;',
        'IRT' => '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
        'ISK' => 'kr.',
        'JEP' => '&pound;',
        'JMD' => '&#36;',
        'JOD' => '&#x62f;.&#x627;',
        'JPY' => '&yen;',
        'KES' => 'KSh',
        'KGS' => '&#x441;&#x43e;&#x43c;',
        'KHR' => '&#x17db;',
        'KMF' => 'Fr',
        'KPW' => '&#x20a9;',
        'KRW' => '&#8361;',
        'KWD' => '&#x62f;.&#x643;',
        'KYD' => '&#36;',
        'KZT' => 'KZT',
        'LAK' => '&#8365;',
        'LBP' => '&#x644;.&#x644;',
        'LKR' => '&#xdbb;&#xdd4;',
        'LRD' => '&#36;',
        'LSL' => 'L',
        'LYD' => '&#x644;.&#x62f;',
        'MAD' => '&#x62f;.&#x645;.',
        'MDL' => 'MDL',
        'MGA' => 'Ar',
        'MKD' => '&#x434;&#x435;&#x43d;',
        'MMK' => 'Ks',
        'MNT' => '&#x20ae;',
        'MOP' => 'P',
        'MRO' => 'UM',
        'MUR' => '&#x20a8;',
        'MVR' => '.&#x783;',
        'MWK' => 'MK',
        'MXN' => '&#36;',
        'MYR' => '&#82;&#77;',
        'MZN' => 'MT',
        'NAD' => '&#36;',
        'NGN' => '&#8358;',
        'NIO' => 'C&#36;',
        'NOK' => '&#107;&#114;',
        'NPR' => '&#8360;',
        'NZD' => '&#36;',
        'OMR' => '&#x631;.&#x639;.',
        'PAB' => 'B/.',
        'PEN' => 'S/.',
        'PGK' => 'K',
        'PHP' => '&#8369;',
        'PKR' => '&#8360;',
        'PLN' => '&#122;&#322;',
        'PRB' => '&#x440;.',
        'PYG' => '&#8370;',
        'QAR' => '&#x631;.&#x642;',
        'RMB' => '&yen;',
        'RON' => 'lei',
        'RSD' => '&#x434;&#x438;&#x43d;.',
        'RUB' => '&#8381;',
        'RWF' => 'Fr',
        'SAR' => '&#x631;.&#x633;',
        'SBD' => '&#36;',
        'SCR' => '&#x20a8;',
        'SDG' => '&#x62c;.&#x633;.',
        'SEK' => '&#107;&#114;',
        'SGD' => '&#36;',
        'SHP' => '&pound;',
        'SLL' => 'Le',
        'SOS' => 'Sh',
        'SRD' => '&#36;',
        'SSP' => '&pound;',
        'STD' => 'Db',
        'SYP' => '&#x644;.&#x633;',
        'SZL' => 'L',
        'THB' => '&#3647;',
        'TJS' => '&#x405;&#x41c;',
        'TMT' => 'm',
        'TND' => '&#x62f;.&#x62a;',
        'TOP' => 'T&#36;',
        'TRY' => '&#8378;',
        'TTD' => '&#36;',
        'TWD' => '&#78;&#84;&#36;',
        'TZS' => 'Sh',
        'UAH' => '&#8372;',
        'UGX' => 'UGX',
        'USD' => '&#36;',
        'UYU' => '&#36;',
        'UZS' => 'UZS',
        'VEF' => 'Bs F',
        'VND' => '&#8363;',
        'VUV' => 'Vt',
        'WST' => 'T',
        'XAF' => 'Fr',
        'XCD' => '&#36;',
        'XOF' => 'Fr',
        'XPF' => 'Fr',
        'YER' => '&#xfdfc;',
        'ZAR' => '&#82;',
        'ZMW' => 'ZK',
	);
	return ! empty( $symbol[ $to ] ) ? $symbol[ $to ] : '$';
}
